package com.twuc.webApp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest
public abstract class ApiTestBase {
    @Autowired
    private MockMvc mockMvc;

    protected MockMvc getMockMvc() {
        return mockMvc;
    }
}