package com.twuc.webApp.domain;

import java.util.ArrayList;
import java.util.List;

public class NoteData {
    private static List<Note> notes = new ArrayList<>();

    public NoteData() {
        notes.add(new Note(1L, "java", "java is language"));
        notes.add(new Note(2L, "html", "html is language"));
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void saveNotes(Note note) {
        notes.add(note);
    }
    public Note getNoteByNoteId(Long postId){
        return notes.stream()
                .filter(note -> note.getId().equals(postId))
                .findFirst()
                .get();
    }
    public void deleteNote(Note note) {
        notes.remove(note);
    }

}
