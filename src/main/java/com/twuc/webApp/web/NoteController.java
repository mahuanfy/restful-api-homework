package com.twuc.webApp.web;

import com.twuc.webApp.domain.Note;
import com.twuc.webApp.service.NoteService;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/notes")
public class NoteController {
    private final NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public ResponseEntity<Resources<Note>> getNotes() {
        Link selfRel = linkTo(methodOn(NoteController.class).getNotes()).withSelfRel();
        Link deleteRel = linkTo(methodOn(NoteController.class).deleteNote(null, null)).withRel("deleteNote");
        Link creteRel = linkTo(methodOn(NoteController.class).createNote(null)).withRel("createNote");
        List<Note> notes = noteService.getNotes();
        Resources<Note> resources = new Resources<>(notes, selfRel, deleteRel, creteRel);

        return ResponseEntity.ok().body(resources);
    }

    @GetMapping(path = "/titles", produces = {"application/hal+json;charset=UTF-8"})
    public ResponseEntity<List<String>> getTitles() {
        List<String> titles = noteService.getTitles();
        return ResponseEntity.ok().body(titles);
    }

    @PostMapping()
    public ResponseEntity<Resource> createNote(@RequestBody @Valid Note note) {

        noteService.createNote(note);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{noteId}")
    public ResponseEntity deleteNote(@PathVariable Long noteId, @RequestBody(required = false) String password) {
        noteService.deleteNoteByNoteId(noteId, password);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
