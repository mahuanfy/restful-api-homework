package com.twuc.webApp.service;

import com.twuc.webApp.domain.Note;
import com.twuc.webApp.domain.NoteData;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class NoteService {
    private NoteData noteData = new NoteData();

    public List<Note> getNotes() {
        return noteData.getNotes();
    }

    public List<String> getTitles() {
        List<Note> notes = noteData.getNotes();
        return notes.stream()
                .map(Note::getTitle)
                .collect(Collectors.toList());
    }

    public void createNote(Note note) {
        noteData.saveNotes(note);
    }

    public void deleteNoteByNoteId(Long noteId, String password) {
        Note note = noteData.getNoteByNoteId(noteId);
        if(isCorrectPassword(password, note)){
            noteData.deleteNote(note);
        }
    }

    private boolean isCorrectPassword(String password, Note note) {
        return Objects.isNull(note.getPassword()) || password.equals(note.getPassword());
    }
}
